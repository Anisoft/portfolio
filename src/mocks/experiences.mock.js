export let experiences = [
    {
        id: 1,
        intitule: "Développeur FullStack - Freelance",
        boutique: "HOTSHI D.A.C",
        logo: "hotshi",
        pays: "Irland",
        dateDebut: "Nov. 2021",
        dateFin: "Présent",
        travaux: [
            "Développement du site vitrine de HOTSHI D.A.C (www.hotshi.com)",
            "Implémentation des modèles des emails automatiques utilisés par l'application mobile Hotshi",
            "Développement de la plateforme d'administration de l'application Hotshi et du réseau social Hotshi"
        ],
        outilsLangage: "Laravel 8, Bootstrap 4, Javascript, SQL, OpenProject, GitLab"
    },
    {
        id: 2,
        intitule: "Développeur d'application JEE",
        boutique: "MEDIA SOFT",
        logo: "mediasoft",
        pays: "Togo",
        dateDebut: "Mar. 2018",
        dateFin: "Aoû. 2021",
        travaux: [
            "Project Owner sur l'intégration de nouvelles institutions à la plateforme de finance digitale BINDOO (au Togo et au Benin)",
            "Implémentation de plusieurs fonctionnalités (interfaces, web services, modules USSD avec le design pattern state, etc.) sur de la plateforme web de finance digitale BINDOO (www.ebindoo.com)",
            "Développement et intégration de la fonctionnalité de génération de rapport (liasse ficale) et de bureau de change dans l'application de gestion des institutions financères (Microfina++) déployé chez plusieurs clients des pays d'Afrique (Togo, Bénin, Niger, RDC, Mauritanie)",
            "Développement de la plateforme d'administration et de génération de casier judiciaire dans le projet d'informatisation de la délivrance du casier judiciaire au Togo (https://casierjudiciaire.gouv.tg)",
        ],
        outilsLangage: "JEE, Javascript, PostgreSQL, Primefaces, Bootstrap 4, OpenProject, GitLab"
    },
    {
        id: 3,
        intitule: "Formateur en JEE",
        boutique: "MEDIA SOFT",
        logo: "mediasoft",
        pays: "Togo",
        dateDebut: "Jan. 2021",
        dateFin: "",
        travaux: [
            "Formation donnée à un groupe de développeurs du ministère de la justice en vue de prendre la main sur la plateforme de casier judiciaire"
        ],
        outilsLangage: "JEE"
    },
    {
        id: 4,
        intitule: "Stagiaire au Woelabs sur un projet environnemental (SCOPE : Sorting and Collecting Plastics in Our Environment)",
        boutique: "WOELABS,",
        logo: "woelab",
        pays: "Togo",
        dateDebut: "Jan. 2017",
        dateFin: "Mar. 20218",
        travaux: [
            "Développeur PHP, Arduino",
            "Focus projet"
        ],
        outilsLangage: "PHP, Bootstrap 3, SQL, Arduino, GitLab, Ms Office, Talend"
    }
]