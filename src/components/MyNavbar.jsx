import { Navbar, Container, Nav, Form, FormControl, Button} from 'react-bootstrap'
import { FaInfoCircle, FaSearch } from 'react-icons/fa';
import './css/MyNavbar.css'

export function MyNavbar () {
    return (
        <Navbar id="nav" bg="dark" expand="lg" style={{ color: '#fff' }}>
            <Container fluid>
                <Navbar.Brand href="/">ANISOFT</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                <Nav className="me-auto my-2 my-lg-0" style={{ maxHeight: '100px' }} navbarScroll >
                    <Nav.Link href="/">Accueil</Nav.Link>
                    <Nav.Link href="/experiences">Expériences Pro</Nav.Link>
                    <Nav.Link href="/formations">Formations</Nav.Link>
                    <Nav.Link href="/recherches">Recherches</Nav.Link>
                    <Nav.Link href="/enseignements">Enseignements</Nav.Link>
                </Nav>
                <Form className="d-flex">
                    <FormControl type="search" placeholder="Rechercher" className="me-2" aria-label="Search" />
                    <Button variant="outline-success"><FaSearch/></Button>
                </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}