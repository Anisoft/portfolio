import React, { useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

import './Navbar.css'

export default function Navbar() {

    const [toggleMenu, setToggleMenu] = useState(false);
    const [largeur, setLargeur] = useState(window.innerWidth);

    const toggleNavSamllScreen = () => {
        setToggleMenu(!toggleMenu);
    }

    useEffect(() => {

        const changeWidth = () => {
            setLargeur(window.innerWidth);
            if(window.innerWidth > 500) {
                setToggleMenu(false);
            }
        }

        window.addEventListener('resize', changeWidth);
        
        return () => {
            window.removeEventListener('resize', changeWidth);
        }
    }, [])

    return (
        <nav>
            { (toggleMenu || largeur > 500) && (
                 <ul className="liste">
                    <li className="items"><Link to="/">Accueil</Link></li>
                    <li className="items"><Link to="/experiences">Expériences Pro</Link></li>
                    <li className="items"><Link to="/formations">Formations</Link></li>
                    <li className="items"><Link to="/recherches">Recherches</Link></li>
                    <li className="items"><Link to="/enseignements">Enseignements</Link></li>
                </ul>
            )}
            <button onClick={ toggleNavSamllScreen }  className="btnToggle">BTN</button>
        </nav>
    )
}
