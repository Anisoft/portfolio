import { useState } from "react";
import { Container, Card, Button } from "react-bootstrap";
import { experiences as experiencesMock} from "../mocks/experiences.mock";
import './css/Experiences.css'

import mediasoft from '../img/mediasoft.png';
import hotshi from '../img/hotshi.png';
import woelab from '../img/woelab.jpeg';

export const Experiences = () => {

    const [experiences, setExperiences] = useState(experiencesMock);

    return (
        <Container>
            <h2>Mes Experiences</h2>
            {
            experiences.map((experience) => {
              return (
                <Card key={ experience.id } className="bg-dark text-white" style={{ marginBottom: '1rem' }}>
                <Card.Body>
                  <Card.Title>
                  { experience.intitule } <span className="text-muted" style={{ fontSize: '0.8em' }}>({ experience.dateDebut} { experience.dateFin === "" ? "" : " - " + experience.dateFin})</span>                      
                  </Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">{ experience.boutique }, { experience.pays }</Card.Subtitle>
                    <ul> {
                      experience.travaux.map((taf)=> {
                        return <li>{taf}</li>
                      })
                    } </ul>
                  <Card.Subtitle className="mb-2 text-muted">Outils/langages : { experience.outilsLangage }</Card.Subtitle>
                  <Card.Link href="#">Another Link</Card.Link>
                </Card.Body>
              </Card>
              );
            })
            }
        </Container>
    );
}