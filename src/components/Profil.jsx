import { Row, Col, Container } from 'react-bootstrap';
import profil from '../img/profil.jpg';

export const Profil = () => {
    return (
        <div className='mt-3'>
            <Container>
                <Row>
                    <Col xs={12} md={3}>
                        <img width={150} height={150} id='profil' className='img-fluid rounded' src={ profil } alt='Profil'/>
                    </Col>
                    <Col xs={12} md={9}>
                        <span>AGBONON EDAGBEDJI</span><br/>
                        <span>Yao Anicet</span><br/>
                        <span>lanicet17@gmail.com</span><br/>
                        <span>yao-anicet.agbonon-edagbedji@elv.ulco-eilco.fr</span><br/>
                        <span>2B Rue Courtebourne, 62100 Calais</span><br/>
                        <span>07 82 37 47 10</span>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={12} className="mt-3">
                        Je suis un jeune de nationalié togolaise vivant en France et pationné 
                        des nouvelles technologies et pratiques dans le domaine de l'informatique. 
                        Je suis actuellement étudiant en Ingénierie Informatique options Machine 
                        Learning et Big Data en alternance chez Avanade.
                    </Col>
                </Row>
            </Container>
        </div>
    );
}