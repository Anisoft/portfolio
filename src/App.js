import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { MyNavbar as Navbar } from './components/MyNavbar';

import { Container } from 'react-bootstrap';
import { FaSearch } from 'react-icons/fa';

import { Profil } from './components/Profil';
import { Experiences } from './components/Experinces';
import { Formations } from './components/Formations';
import { Recherches } from './components/Recherches';
import { Enseignements } from './components/Enseignements';

function App() {
  return (
      <Router>
        <div className="App">
          <Container>
            <Navbar />
            <div className='contenu'>
              <Routes>
                <Route exact path={'/'} element={<Profil />}/>
                <Route path={'/experiences'} element={<Experiences />}/>
                <Route path={'/formations'}  element={<Formations />} />
                <Route path={'/recherches'}  element={<Recherches />} />
                <Route path={'/enseignements'}  element={<Enseignements />} />
              </Routes>
            </div>
          </Container>
        </div>
      </Router>
  );
}

export default App;
